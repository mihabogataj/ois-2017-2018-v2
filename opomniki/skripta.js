window.addEventListener('load', function() {
	//stran nalozena

	// Izvedi prijavo
	var izvediPrijavo = function() {
	  var uporabnik = document.querySelector("input").value;
	  document.getElementById("uporabnik").innerHTML = uporabnik;
	  document.getElementsByClassName("pokrivalo").item(0).style.visibility = "hidden";
	  console.log(uporabnik);
	}
	
	document.getElementById("prijavniGumb").addEventListener('click', izvediPrijavo);
	
	// Dodaj opomnik
	var dodajOpomnik = function() {
		var naziv_opomnika = document.getElementById("naziv_opomnika").value;
		document.querySelector("#naziv_opomnika").value = "";
		var cas_opomnika = document.getElementById("cas_opomnika").value;
		document.querySelector("#cas_opomnika").value = "";
		var opomniki = document.getElementById("opomniki");
		opomniki.innerHTML += " \
    		<div class='opomnik' class='senca robc'> \
    		<div class='naziv_opomnika'> " + naziv_opomnika + " </div> \
    		<div class='cas_opomnika'> Opomnik čez <span>" + cas_opomnika + "</span> sekund. </div> \
			 </div>";
	}
	document.getElementById("dodajGumb").addEventListener('click', dodajOpomnik);
	


	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
			
			if (cas == 0) {
				alert("Opomnik!\n\nZadolžitev " + opomnik.querySelector(".naziv_opomnika").innerHTML + " je potekla!");
				//document.querySelector("opomnik").removeChild(opomnik);
				opomnik.parentElement.removeChild(opomnik)
			}
			else {
				casovnik.innerHTML = cas - 1;
			}
			
			console.log(opomnik.querySelector(".naziv_opomnika").innerHTML);
		}
	}
	setInterval(posodobiOpomnike, 1000);

});
